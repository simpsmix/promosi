package com.trainingmicro.promo.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import sun.nio.cs.Surrogate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity @Data
public class Product {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty @Size(min = 3, max = 100)
    private String code;

    @NotEmpty @Size(min = 3, max = 255)
    private String name;

    @NotNull @Min(1)
    private BigDecimal price;

}
