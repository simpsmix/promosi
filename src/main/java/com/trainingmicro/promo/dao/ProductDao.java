package com.trainingmicro.promo.dao;

import com.trainingmicro.promo.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository <Product, String> {
}
