package com.trainingmicro.promo.controller;

import com.trainingmicro.promo.dao.ProductDao;
import com.trainingmicro.promo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    @Autowired
    private ProductDao productDao;

    @GetMapping("/product/")
    public Iterable<Product> semuaProduk() {
        return productDao.findAll();
    }
}